const axios = require('axios');
const serverAxios = require('../api/server-axios');
const refreshUserAccessToken = require('../middleware/refreshUserAccessToken');

const appToken = process.env.APP_TOKEN;
const tableId = process.env.TABLE_ID_ORDER;

const orderCtrl = {
    getOrderList: async (req, res) => {
        const orderAPI = `https://open.larksuite.com/open-apis/bitable/v1/apps/${appToken}/tables/${tableId}/records`;
        try {

            const response = await serverAxios.get(orderAPI);
            const orders = response.data.data.items;

            res.json(orders);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    getOrder: async (req, res) => {
        const orderAPI = `https://open.larksuite.com/open-apis/bitable/v1/apps/${appToken}/tables/${tableId}/records/`;
        try {
            // get the order by field orders
            const result = await serverAxios.get(orderAPI);
            const orders = result.data.data.items;
            const order = orders.find(order => order.fields.orders === req.params.id);
            if (!order) {
                return res.status(404).json({ error: 'Order not found' });
            }

            res.json(order);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    createOrder: async (req, res) => {
        const accessToken = await refreshUserAccessToken();
        const orderAPI = `https://open.larksuite.com/open-apis/bitable/v1/apps/${appToken}/tables/${tableId}/records`;
        try {
            // check duplicates
            const result = await serverAxios.get(orderAPI);
            const orders = result.data.data.items;
            const duplicate = orders.find(order => order.fields.orders === req.body.orders);
            if (duplicate) {
                return res.status(201).json({ error: 'Order already exists' });
            }
            // create order
            const response = await axios.post(orderAPI, {
                fields: {
                    ...req.body
                }
            },
                {
                    headers: {
                        "content-type": "application/json; charset=utf-8",
                        "Authorization": "Bearer " + accessToken
                    }
                }
            );
            // console.log(response.data).record;
            const order = response.data.data.record;
            res.json(order);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    // updateOrder: async (req, res) => {
    //     const orderAPI = `https://open.larksuite.com/open-apis/bitable/v1/apps/${appToken}/tables/${tableId}/records/${req.params.id}`;
    //     try {

    //         const response = await serverAxios.put(orderAPI, {
    //             fields: {
    //                 ...req.body
    //             }
    //         });
    //         const order = response.data.data.record;

    //         res.json(order);
    //     } catch (error) {
    //         console.error(error);
    //         res.status(500).json({ error: 'Internal server error' });
    //     }
    // },

    // deleteOrder: async (req, res) => {
    //     const orderAPI = `https://open.larksuite.com/open-apis/bitable/v1/apps/${appToken}/tables/${tableId}/records/${req.params.id}`;
    //     try {

    //         const response = await serverAxios.delete(orderAPI);
    //         const order = response.data.data.record;

    //         res.json(order);
    //     } catch (error) {
    //         console.error(error);
    //         res.status(500).json({ error: 'Internal server error' });
    //     }
    // }

};

module.exports = orderCtrl;