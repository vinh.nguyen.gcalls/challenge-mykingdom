const serverAxios = require('../api/server-axios');

const appToken = process.env.APP_TOKEN;
const tableId = process.env.TABLE_ID_CATEGORTY;

const categoryCtrl = {
    getCategoryList: async (req, res) => {
        const categoryAPI = `https://open.larksuite.com/open-apis/bitable/v1/apps/${appToken}/tables/${tableId}/records`;
        try {

            const response = await serverAxios.get(categoryAPI);

            const categories = response.data.data.items;

            categories.forEach(category => {
                if (category.fields.image_url) {
                    const imageURLs = category.fields.image_url.split(', ').map(url => url.trim());
                    category.fields.image_url = imageURLs;
                }
            });

            res.json(categories);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    getCategory: async (req, res) => {
        const categoryAPI = `https://open.larksuite.com/open-apis/bitable/v1/apps/${appToken}/tables/${tableId}/records/${req.params.id}`;
        try {

            const response = await serverAxios.get(categoryAPI);
            const category = response.data.data.record;
            // console.log(category)

            if (category.fields.image_url) {
                const imageURLs = category.fields.image_url.split(', ').map(url => url.trim());
                category.fields.image_url = imageURLs;
            }

            res.json(category);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },
};

module.exports = categoryCtrl;