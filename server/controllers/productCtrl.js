const serverAxios = require('../api/server-axios');
// const refreshUserAccessToken = require('../middleware/refreshUserAccessToken');

const appToken = process.env.APP_TOKEN;
const tableId = process.env.TABLE_ID_PRODUCT;

const productCtrl = {
    getProductList: async (req, res) => {
        const productAPI = `https://open.larksuite.com/open-apis/bitable/v1/apps/${appToken}/tables/${tableId}/records`;
        try {

            const response = await serverAxios.get(productAPI);
            // const results = await refreshUserAccessToken();
            // console.log("refreshUserAccessToken: ", results);
            const products = response.data.data.items;

            products.forEach(product => {
                if (product.fields.image_url) {
                    const imageURLs = product.fields.image_url.split(', ').map(url => url.trim());
                    product.fields.image_url = imageURLs;
                }
            });

            res.json(products);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    getProduct: async (req, res) => {
        const productAPI = `https://open.larksuite.com/open-apis/bitable/v1/apps/${appToken}/tables/${tableId}/records/${req.params.id}`;
        try {

            const response = await serverAxios.get(productAPI);
            const product = response.data.data.record;
            // console.log(product)

            if (product.fields.image_url) {
                const imageURLs = product.fields.image_url.split(', ').map(url => url.trim());
                product.fields.image_url = imageURLs;
            }

            res.json(product);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    getProductListByCategory: async (req, res) => {
        const productAPI = `https://open.larksuite.com/open-apis/bitable/v1/apps/${appToken}/tables/${tableId}/records`;
        try {

            const response = await serverAxios.get(productAPI);

            const products = response.data.data.items;

            products.forEach(product => {
                if (product.fields.image_url) {
                    const imageURLs = product.fields.image_url.split(', ').map(url => url.trim());
                    product.fields.image_url = imageURLs;
                }
            });

            let categoryProducts = products;

            if (req.params.id) {
                categoryProducts = products.filter(product =>
                    product.fields.category.some(category =>
                        category.includes(req.params.id)
                    )
                );
            }


            res.json(categoryProducts);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    }
};

module.exports = productCtrl;