const router = require('express').Router();
const categoryCtrl = require('../controllers/categoryCtrl');

router.get('/', categoryCtrl.getCategoryList);

router.get('/:id', categoryCtrl.getCategory);


module.exports = router;