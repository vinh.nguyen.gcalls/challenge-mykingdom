const router = require('express').Router();
const orderCtrl = require('../controllers/orderCtrl');

router.get('/', orderCtrl.getOrderList);

router.get('/:id', orderCtrl.getOrder);

router.post('/create', orderCtrl.createOrder);


module.exports = router;