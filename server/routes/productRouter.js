const router = require('express').Router();
const productCtrl = require('../controllers/productCtrl');


router.get('/', productCtrl.getProductList);

router.get('/:id', productCtrl.getProduct);

router.get('/category/:id', productCtrl.getProductListByCategory);


module.exports = router;