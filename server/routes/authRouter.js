const router = require('express').Router();

router.get('/callback', (req, res) => {
    const authCode = req.query.code;

    console.log("authCode: ", authCode);

    res.send('Authentication successful');
});

module.exports = router;