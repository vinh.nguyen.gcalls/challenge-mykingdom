const axios = require('axios');
const getTenantAccessToken = require('../middleware/getTenantAccessToken');
const refreshUserAccessToken = require('../middleware/refreshUserAccessToken');

const axiosServer = axios.create({
    baseURL: "",
    headers: {
        "content-type": "application/json; charset=utf-8",
    },
    responseType: "json",
});


axiosServer.interceptors.request.use(
    async (config) => {
        const accessToken = await getTenantAccessToken();
        config.headers.Authorization = `Bearer ${accessToken}`;
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);


module.exports = axiosServer;