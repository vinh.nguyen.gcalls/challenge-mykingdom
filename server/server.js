require('dotenv').config();
const express = require('express');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

//Routes
app.use('/auth', require('./routes/authRouter'));
app.use('/products', require('./routes/productRouter'));
app.use('/categories', require('./routes/categoryRouter'));
app.use('/orders', require('./routes/orderRouter'));


app.get('/', (req, res) => {
    res.json({ msg: "Welcome to my server app!" });
});

app.listen(port, () => console.log('Server is running on port 5000'));