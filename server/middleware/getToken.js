const axios = require('axios');
const getTenantAccessToken = require('./getTenantAccessToken');

const getToken = async () => {

    const appToken = process.env.APP_TOKEN;
    const tableID = process.env.TABLE_ID_TOKEN;
    const accessToken = await getTenantAccessToken();
    const userAccessTokenEndpoint = `https://open.larksuite.com/open-apis/bitable/v1/apps/${appToken}/tables/${tableID}/records`;

    try {
        const response = await axios.get(userAccessTokenEndpoint,
            {
                headers: {
                    "content-type": "application/json; charset=utf-8",
                    "Authorization": "Bearer " + accessToken
                }
            }
        );

        return response.data.data.items[0];
    } catch (error) {
        console.error(error);
        throw new Error('Error getting access token');
    }
};

module.exports = getToken;