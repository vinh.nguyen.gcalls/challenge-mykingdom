const axios = require('axios');
const getToken = require('./getToken');
const getAppAccessToken = require('./getAppAccessToken');

const refreshUserAccessToken = async () => {

    const appToken = process.env.APP_TOKEN;
    const tableID = process.env.TABLE_ID_TOKEN;
    const appAccessToken = await getAppAccessToken();
    const results = await getToken();
    const accessToken = results.fields.access_token;
    const refreshToken = results.fields.refresh_token;
    const userAccessTokenEndpoint = `https://open.larksuite.com/open-apis/authen/v1/refresh_access_token`;

    try {
        const response = await axios.post(userAccessTokenEndpoint, {
            grant_type: "refresh_token",
            refresh_token: refreshToken
        }, {
            headers: {
                "content-type": "application/json; charset=utf-8",
                "Authorization": "Bearer " + appAccessToken
            }
        }
        );

        const newAccessToken = response.data.data.access_token;
        const newRefreshToken = response.data.data.refresh_token;

        const updateTokenEndpoint = `https://open.larksuite.com/open-apis/bitable/v1/apps/${appToken}/tables/${tableID}/records/${results.id}`;

        const updateResponse = await axios.put(updateTokenEndpoint, {
            fields: {
                access_token: newAccessToken,
                refresh_token: newRefreshToken
            }
        },
            {
                headers: {
                    "content-type": "application/json; charset=utf-8",
                    "Authorization": "Bearer " + appAccessToken
                }
            }
        );

        return newAccessToken;
    } catch (error) {
        console.error(error);
        throw new Error('Error refreshing access token');
    }

};

module.exports = refreshUserAccessToken;