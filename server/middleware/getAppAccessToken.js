const axios = require('axios');

const getAppAccessToken = async () => {
    const appAccessTokenEndpoint = 'https://open.larksuite.com/open-apis/auth/v3/app_access_token/internal';
    const appID = process.env.APP_ID;
    const appSecret = process.env.APP_SECRET;

    try {
        const response = await axios.post(appAccessTokenEndpoint,
            {
                app_id: appID,
                app_secret: appSecret,
            }
        );

        return response.data.app_access_token;
    } catch (error) {
        console.error(error);
        throw new Error('Error getting access token');
    }
};

module.exports = getAppAccessToken;