const axios = require('axios');
const getAppAccessToken = require('./getAppAccessToken');

const getCodeRedirectUrl = async () => {
    const redirectUri = encodeURIComponent('http://localhost:5000/auth/callback');
    const appID = process.env.APP_ID;

    const codeRedirect = `https://open.larksuite.com/open-apis/authen/v1/index?redirect_uri=${redirectUri}&app_id=${appID}`;

    try {
        const appAccessToken = await getAppAccessToken();

        const getCodeResponse = await axios.get(codeRedirect, {
            headers: {
                Authorization: `Bearer ${appAccessToken}`
            }
        });
        const url = getCodeResponse.request.res.responseUrl;

        return url;

    } catch (error) {
        console.error(error);
        throw new Error('Error getting code redirect');
    }
};

module.exports = getCodeRedirectUrl;